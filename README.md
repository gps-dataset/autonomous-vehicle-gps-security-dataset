# Unmanned and Autonomous Vehicle GPS Security Dataset

This dataset repository contains data log information of an Unmanned Ground Vehicle (UGV) testbed guided by Global Positioning Systems (GPS). The vehicle collected both Normal data using authentic signals from satellites and real-life GPS spoofing attack data using a GPS spoofer. GPS spoofing attacks have been performed under the influence of authentic GPS signals from satellites. The repository contains 6 files: 

## Full_Dataset.csv

This is the main dataset that contains 62,042 entries of both normal data and GPS spoofing attack data. The data were collected from The University of Arizona Campus, Tucson, AZ, USA. The dataset is pre-processed and can be directly used in any machine learning based applications.

## Normal_Dataset.csv

This is the normal data entries from the Full_Dataset. This normal data can be used to train any one class classifier. This dataset is also useful for Novelty-based intrusion detection systems where the normal behavior is modeled using the regular normal data. 

## Attack_Dataset.csv

This is the GPS spoofing attack data from the Full_Dataset. This dataset can be used to test a GPS spoofing attack detection System. Also, it can be used to train a one class classifier for an Anomaly-based intrusion detection system. It should be noted that this attack dataset contains only a GPS spoofing attack scenario, which may not always be effective in modeling the overall GPS attack behavior, but can be useful to model a Anomaly-based GPS spoofing attack detection system. 

## Extended_Test_Full_Dataset.csv

This is a secondary dataset that contains 6,890 entries of both normal data and GPS spoofing attack data. The data were collected from a different location which is approximately 3.8 Kilometers away from the University of Arizona campus. This dataset can be used to train/ test a machine learning based model with different location data to increase the generalizability of the model. If a machine learning model is trained and tested using data from only one location, testing it with a different location data may give poor performance since the model will detect any GPS location other than the trained location as attack data. 

## Extended_Test_Normal_Dataset.csv

This file contains the normal data entries from the Extended_Test_Full_Dataset. 

## Extended_Test_Attack_Dataset.csv

This file contains the attack data entries from the Extended_Test_Full_Dataset. 
